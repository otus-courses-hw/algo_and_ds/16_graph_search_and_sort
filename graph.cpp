#include "graph.hpp"
#include <algorithm>
#include <functional>

graph::graph(unsigned vertices) :
    table(vertices)
{
    for (auto &row : table)
        row.resize(vertices, false);
}

void graph::add_edge(std::pair<unsigned, unsigned> vertices)
{
    table.at(vertices.first).at(vertices.second) = true;
}

auto graph::sort_demucron() const -> result
{
    std::vector<long> half_in(table.size(), 0);

    for (const auto &row : table)
    {
        for (std::size_t j = 0; j < row.size(); ++j)
        {
            if (row.at(j))
                half_in.at(j)++;
        }
    }

    auto error = sort_error::none;
    std::list<unsigned> sorted;
    while(true)
    {
        std::list<unsigned > zeroes;
        unsigned negatives = 0;

        for (std::size_t i = 0; i < half_in.size(); ++i)
        {
            if (half_in.at(i) < 0)
                ++negatives;

            if (half_in.at(i) == 0)
                zeroes.push_back(i);
        }

        if (negatives == half_in.size())
            break; // reached the end, that is OK

        if (zeroes.empty())
        {
            error = sort_error::circular_dependencies;
            break; // should be an error
        }

        for (auto vertex : zeroes)
        {
            sorted.push_back(vertex);
            auto &row = table.at(vertex);
            std::transform(half_in.cbegin(), half_in.cend(),
                           row.cbegin(), half_in.begin(),
                           std::minus<>{});

            --half_in.at(vertex); // exclude found zeroes
        }
    }

    return {sorted, error};
}

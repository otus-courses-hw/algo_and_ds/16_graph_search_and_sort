#include <utility>
#include <vector>
#include <list>

enum class sort_error {none, circular_dependencies};

class graph
{
    private:
        using adjacent_matrix = std::vector<std::vector<bool>>;
        using result = std::pair<std::list<unsigned>, sort_error>;
        adjacent_matrix table;

    public:
        graph() = delete;
        ~graph() = default;
        explicit graph(unsigned vertices);

        void add_edge(std::pair<unsigned, unsigned> vertices);
        [[nodiscard]] result sort_demucron() const;
};

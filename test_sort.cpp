#include <catch2/catch.hpp>
#include "graph.hpp"

TEST_CASE("happy pass")
{
    unsigned vertices = 10;
    graph gr(vertices);
    gr.add_edge({0, 1});
    gr.add_edge({1, 4});
    gr.add_edge({2, 3});
    gr.add_edge({3, 0});
    gr.add_edge({3, 1});
    gr.add_edge({3, 4});
    gr.add_edge({3, 5});
    gr.add_edge({4, 6});
    gr.add_edge({5, 4});
    gr.add_edge({5, 7});
    gr.add_edge({6, 7});
    gr.add_edge({8, 9});
    /*         GRAPH
     *      0---x1
     *      x   x|
     *      |  / |     8
     *      | /  x     |
     * 2---x3---x4     |
     *      |   x|     x
     *      |  / |     9
     *      x /  x
     *      5    6
     *       \  /
     *        xx
     *         7
     */
    auto [order, error] = gr.sort_demucron();
    CHECK(error == sort_error::none);
    CHECK(order == std::list<unsigned> {2,8,3,9,0,5,1,4,6,7});
}

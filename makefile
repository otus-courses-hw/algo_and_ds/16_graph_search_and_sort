all: test

test: test_main.o test_sort.o graph.o
	g++ -g -O0 -std=c++20 -o $@ $^

%.o: %.cpp
	g++ -g -Werror -O0 -std=c++20 -o $@ -c $<

test_sort.o: test_main.cpp graph.cpp

.PHONY: clean run

clean:
	rm *.o test

run:
	./test
